Test Case 1
Goal: Sign Up
Initial State: User starts up the Walkiki app.
Final State: User is presented with home screen.

    1)  User sees start screen and sees "Log In" and "Sign Up" buttons
    2)  User presses the "Sign Up" button
    3)  User presented with input for "Name", "Email", "Password", "Enter", "Back"
            1) User can press the "Back" button to return to start screen
    4)  User presses the enter
            1)  Sends info to the server
            2)  Server validates it
                1) Bad data
                2) (error - return user to step 3, re-input data)
            3)  Store user info
    5) User is shown the introduction video
    6) User presented with the home screen


Test Case 2
Goal: Log In
Initial State: User starts up the Walkiki app.
Final State: User Presented with home screen.
    
    1)  User sees start screen and sees "Log In" and "Sign Up" buttons
    2)  User presses the "Log In" button
    3)  User presented with input for "Username", "Password", "Enter", "Back"
            1) User can press the "Back" button to return to start screen
    4)  User fills in credentials and presses enter
            1)  Sends info to the server
            2)  Server validates User (error - bad credentials, go back to step 3)
    5)  User is presented with home screen
 
