Test Case 26:
Goal: Find a community
Start state: Community Main Screen
End state: Community page

1) User is presented with the community main page and options "My Communities", "Find Community", "Create Community", "Back"
2) User presses "My Communities"
3) User is presented with a search box with a list of all communities the user belongs to and a "Back" button
   1) User starts to input a community name, list displays joined communities based on current characters entered
   2) Once user is done, only the matching communities are displayed
   3) If user presses search again, the previous search is cleared
   4) User can press the "Back" button to return to the community main screen.
4) User is taken to the community's page.

Test case 27:
Goal: Join a community
Start State: Community Main Screen
End state: Community Main Screen

1) User is presented with the community main page and options "My Communities", "Find Community", "Create Community", "Back"
2) User presses "Find Community" button
3) User is presented with a list of all local communities with a search box and a drop-down for search ordering for "Popularity", "Average Walk Rating", "Number of walks"
   1) User starts to input a community name, list displays local communities based on current characters entered
   2) Once user is done, only the matching communities are displayed
   3) If user presses search again, the previous search is cleared
   4) User can press the "Back" button to return to the home screen
   5) User can order search results by a dropdown for "Popularity", "Average Walk Rating", "Number of walks"
4) User is taken to a descriptor page (name, description, total members, number of walks, average walk rating) for the community with a "Join" button and a "Cancel" button
   1) If user selects "Join", data is sent to the server and the community is added to "My Communities"
   2) If user selects "Cancel", user is taken back to the community main screen.
5) User returns to the community main screen



Test Case 28:
Goal: Create a community
Start State: Community Main Screen
End State: Community page

1) User is presented with the community main page and options "My Communities", "Find Community", "Create Community", "Back"
2) User presses "Create Community" button.
3) User is presented with input "Community Name", "Community Description"
   1) User can press the "Back" button to return to the Community main screen
4) User presses enter
      1)  Sends info to the server
      2)  Server confirms data
         1) Bad data
         2) (error - return user to step 3, re-input data)
      3)  User's community is created
5) User is presented with the community's page

Test Case 29:
Goal: Return to Home Screen
Start State: Community Main Screen
End State: Home Screen

1) User is presented with the community main page and options "My Communities", "Find Community", "Create Community", "Back"
2) User selects "Back"
3) User is returned to the Home Screen